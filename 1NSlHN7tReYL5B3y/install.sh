:
#
# Kopiering av specialfiler - skall till ../server/docroot
#
# @(#)Fenix/wsdl/fenix/install.sh	1.3 2015-07-15
#

FILES="common.xsd"

DIR=../server/docroot

[ ! -d $DIR ] && mkdir $DIR

for i in $FILES; do
	cp -f $i $DIR/$i
done
